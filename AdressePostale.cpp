#include "AdressePostale.h"

AdressePostale::AdressePostale(string Lib, string Comp, string Code, string V)
    :Adresse(Lib,Comp, Code, V)
{

}

AdressePostale::~AdressePostale()
{
    //dtor
}

string AdressePostale::toString()
{

    ostringstream oss;
    oss << "Libelle :" << this->GetLibelle() << endl;
    oss << "Complement :" << this->GetComplement() << endl;
    oss << "Code postal :" << this->GetCodePostale() << endl;
    oss << "Ville :" << this->GetVille() << endl;
    return oss.str();

}
