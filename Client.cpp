#include "Client.h"

Client::Client(int Id, string N, string M, string L, string Comp, string Code, string V)
{
    this->SetIdentifiant(Id);
    this->SetNom(N);
    this->AdressePostale = new Adresse(L,Comp,Code,V);
    this->SetMail(M);
}

Client::~Client()
{
    cout << "Destruction (Client) : " << endl;
    if (AdressePostale != nullptr)
    {
        delete AdressePostale;
    }
}

string Client::toString()
{

 ostringstream oss;
 oss << this->AdressePostale->toString() << endl;
 oss << "Identifiant : " <<this->GetIdentifiant()  << endl;
 oss << "Nom : " << this->GetNom() << endl;
 oss << "Mail : " << this->GetMail() << endl;
 oss << "****************************************"<<endl<<endl;
    return oss.str();
}
