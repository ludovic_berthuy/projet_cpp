#ifndef OPERATION_H
#define OPERATION_H
#include <iostream>

using namespace std;

class Operation
{
    public:
        /** Default constructor */
        Operation(double=0, double=0, double=0);
        //rajouter la date pour identifier l'opération
        /** Default destructor */
        ~Operation();

        double GetTotal_CB() { return total_CB; }
        void SetTotal_CB(double val) { total_CB = val; }
        double GetTotal_Retrait() { return Total_Retrait; }
        void SetTotal_Retrait(double val) { Total_Retrait = val; }
        double GetTotal_Depot() { return Total_Depot; }
        void SetTotal_Depot(double val) { Total_Depot = val; }
        double GetSolde_Total() { return Solde_Total; }
        void AjouteRoperation(int,double);

    protected:

    private:
        double total_CB=0;
        double Total_Retrait=0;
        double Total_Depot=0;
        double Solde_Total=0;

};

#endif // OPERATION_H
