#include "Professionnels.h"

Professionnels::Professionnels(int Id, string N, string M, string L, string Comp, string Code, string V, string S, string Statut, string L2, string Comp2, string Code2, string V2)
    :Client(Id, N, M, L, Comp, Code, V)
{

    this->AdresseSiege = new Adresse(L2,Comp2,Code2,V2);
    this->SetSiret(S);
    this->SetStatut(Statut);

    //ctor
}


Professionnels::~Professionnels()
{
    cout << "Destruction (Professionel) :" <<endl;
    if (AdresseSiege != nullptr)
    {
        delete AdresseSiege;
    }
}


string Professionnels::toString()
{

 ostringstream oss;
 oss << "Adresse du siege :" << this->AdresseSiege->toString() << endl;
 oss << "Siret : " <<this->GetSiret() << endl;
 oss << "Statut juridique :" << this->GetStatut() << endl;
 oss << "****************************************"<<endl<<endl;
    return oss.str();
}
