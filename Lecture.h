#ifndef LECTURE_H
#define LECTURE_H
#include<iostream>
#include <fstream>
#include "Operation.h"
#include "GestionClient.h"
#include <iomanip>
#include <map>
#include<sstream>
using namespace std;

class Lecture
{
    public:
        /** Default constructor */
        Lecture();
        /** Default destructor */
        ~Lecture();

        void LireFichier();
        void LogAnomalies(string);
        string toString();
    protected:

    private:
ostringstream oss;

};

#endif // LECTURE_H
