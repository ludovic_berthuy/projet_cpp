#include "Adresse.h"

Adresse::Adresse()
{
    //ctor
}
Adresse::Adresse(string Lib, string Comp, string Code, string V)
{
    this->SetLibelle(Lib);
    this->SetComplement(Comp);
    this->SetCodePostale(Code);
    this->SetVille(V);
}
Adresse::~Adresse()
{
    //dtor
}
string Adresse::toString()
{

 ostringstream oss;
 oss << "Libelle :" << this->GetLibelle() << endl;
 oss << "Complement :" << this->GetComplement() << endl;
 oss << "Code postal :" << this->GetCodePostale() << endl;
 oss << "Ville :" << this->GetVille() << endl;
    return oss.str();
}
