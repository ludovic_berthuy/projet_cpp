#include "GestionClient.h"

GestionClient::GestionClient()
{
    mapClients.clear();
}

GestionClient::~GestionClient()
{
    cout << "Destruction vecteur de clients" <<endl;
    mapClients.clear();
}

void GestionClient::Ajouter(Client* newClient)
{
    mapClients.insert(pair<int,Client*>(newClient->GetIdentifiant(), newClient));
}

string GestionClient::AfficherTout()
{

 ostringstream oss;


    for (map<int,Client*>::iterator it=mapClients.begin(); it!=mapClients.end(); ++it)
    {oss << it->second->toString();
    oss << endl <<"****************************************" << endl;
    }
    return oss.str();
}

