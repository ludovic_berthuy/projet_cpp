#ifndef CLIENT_H
#define CLIENT_H
#include <iostream>
#include <string>
#include <sstream>
#include "adresse.h"
#include "MonException.h"
using namespace std;

class Client
{
    public:
        Client();
        Client(int =0, string ="\0", string="\0", string ="\0", string ="\0", string="\0", string="\0");
        ~Client();

        int GetIdentifiant() { return Identifiant; }
        void SetIdentifiant(int val) { Identifiant = val; }
        string GetNom() { return Nom; }
        void SetNom(string val)
        {
            if (val.length() >50)
            {
                throw MonException(5);
            }

        Nom = val;
        }
        Adresse GetAdressePostale(){return *AdressePostale;}
//        void SetAdressePostale(Adresse addr) {AdressePostale =addr};
        string GetMail() { return Mail;}
        void SetMail(string val)
        {
            int found = val.find('@');
            if(found>val.length())
            {
                throw MonException(6);
            }
        Mail = val;
        }

        virtual string toString();
    protected:

    private:
        int Identifiant;
        string Nom;
        Adresse *AdressePostale=nullptr;
        string Mail;
};

#endif // CLIENT_H
