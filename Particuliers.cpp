#include "Particuliers.h"

 Particuliers::Particuliers(int Id, string N, string M, string L, string Comp, string Code, string V, string dNaiss, string Pre, string Se)
    :Client(Id, N, M, L, Comp, Code, V)
{

    this->SetDateDeNaissance(dNaiss);
    this->SetPrenom(Pre);
    this->SetSexe(Se);

    //ctor
}

Particuliers::~Particuliers()
{
    cout << "Destruction (Particulier) : " << endl;

}

string Particuliers::toString()
{

 ostringstream oss;
 oss << "Date de naissance : " << this->GetDateDeNaissance() << endl;
 oss << "Prenom : " <<this->GetPrenom() << endl;
 oss << "Sexe : " << this->GetSexe() << endl;
 oss << "****************************************"<<endl<<endl;
    return oss.str();
}
