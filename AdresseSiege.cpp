#include "AdresseSiege.h"

AdresseSiege::AdresseSiege(string Lib, string Comp, string Code, string V)
    :Adresse(Lib,Comp, Code, V)
{
    //ctor
}

AdresseSiege::~AdresseSiege()
{
    //dtor
}

string AdresseSiege::toString()
{

    ostringstream oss;
    oss << "Libelle :" << this->GetLibelle() << endl;
    oss << "Complement :" << this->GetComplement() << endl;
    oss << "Code postal :" << this->GetCodePostale() << endl;
    oss << "Ville :" << this->GetVille() << endl;
    return oss.str();


}
