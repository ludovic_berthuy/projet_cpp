#ifndef ADRESSE_H
#define ADRESSE_H
#include <iostream>
#include <sstream>
using namespace std;

class Adresse
{
    public:
        Adresse();
        Adresse(string="/0", string="/0", string="/0", string="/0");
        virtual ~Adresse();

        string GetLibelle() { return Libelle; }
        void SetLibelle(string val) { Libelle = val; }
        string GetComplement() { return Complement; }
        void SetComplement(string val) { Complement = val; }
        string GetCodePostale() { return CodePostal; }
        void SetCodePostale(string val) { CodePostal = val; }
        string GetVille() { return Ville; }
        void SetVille(string val) { Ville = val; }

        virtual string toString();
    protected:

    private:
        string Libelle;
        string Complement;
        string CodePostal;
        string Ville;
};

#endif // ADRESSE_H
