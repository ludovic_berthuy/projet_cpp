#ifndef GESTIONCLIENT_H
#define GESTIONCLIENT_H

#include <map>
#include "Client.h"
class GestionClient
{
    public:
        GestionClient();
        virtual ~GestionClient();
        int GetnbClient(){return mapClients.size();}
        void Ajouter(Client*);
        string AfficherTout();
        Client GetClient(int nbClients)
        {
            return *mapClients.at(nbClients);
        }

    protected:

    private:
        map<int,Client*> mapClients;
};

#endif // GESTIONCLIENT_H
