#ifndef PARTICULIERS_H
#define PARTICULIERS_H

#include <Client.h>


class Particuliers : public Client
{
    public:
        Particuliers(int =0, string ="\0", string="\0", string ="\0", string ="\0", string="\0", string="\0",string="\0",string="\0",string ="\0" );
        ~Particuliers();

        string GetDateDeNaissance() { return DateDeNaissance; }
        void SetDateDeNaissance(string val) { DateDeNaissance = val; }
        string GetPrenom() { return Prenom; }
        void SetPrenom(string val)
        {
            if (val.length() > 50)
            {
                throw MonException(1);
            }

            Prenom = val;
        }
        string GetSexe() { return Sexe; }
        void SetSexe(string val)
        {
        if (val!="Masculin" && val!="F�minin")
            throw MonException(2);
        Sexe = val;

        }

        virtual string toString() override ;
    protected:

    private:
        string DateDeNaissance;
        string Prenom;
        string Sexe;
};

#endif // PARTICULIERS_H
