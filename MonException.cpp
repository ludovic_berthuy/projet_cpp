#include "MonException.h"

MonException::MonException(int err)throw()
    :exception()
{
    codeErr=err;
}

MonException::~MonException() throw()
{
    //dtor
}
// informe sur la nature de l'erreur
string MonException::Getmessage() const
{
    switch (this->codeErr)
    {
        case 1:
            message = "prenom trop long!";
            break;

        case 2:
            message = "Erreur dans la saisie du sexe !";
            break;
        case 3:
            message = "Siret de plus de 14 chiffre !";
            break;
        case 4:
            message = "Statut juridique non conforme!";
            break;
        case 5:
            message = "Nom sup�rieur � 50 caract�res!";
            break;
        case 6:
            message = "il n'y a pas de @ dans le mail";
            break;
        default:
            message = "Autre Erreur !!";
            break;
    }


    return message;

}

const char* MonException::what() const throw()
{
    Getmessage();
    return message.c_str();
}


