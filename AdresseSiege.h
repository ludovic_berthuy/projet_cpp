#ifndef ADRESSESIEGE_H
#define ADRESSESIEGE_H

#include <Adresse.h>


class AdresseSiege : public Adresse
{
    public:
        AdresseSiege(string, string, string, string);
        virtual ~AdresseSiege();
    /* virtual */ string toString() override;
    protected:

    private:
};

#endif // ADRESSESIEGE_H
