#ifndef MONEXCEPTION_H
#define MONEXCEPTION_H

#include <exception>
#include <iostream>

using namespace std;

class MonException : public exception
{
    public:
        MonException(int) throw();
        virtual ~MonException() throw();
        string Getmessage() const ;
        const char* what() const throw();

    protected:

    private:
        mutable string message;
        int codeErr;
};

#endif // MONEXCEPTION_H
