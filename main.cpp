#include <iostream>
#include "Adresse.h"
#include "AdressePostale.h"
#include "AdresseSiege.h"
#include "client.h"
#include "Particuliers.h"
#include "Professionnels.h"
#include "MonException.h"
#include "GestionClient.h"
#include <exception>
#include <map>
#include "Lecture.h"
#include "Operation.h"

using namespace std;

int main()
{

try{

// Vecteur de Clients
GestionClient ListeClients;

// Saisie des informations des clients

    Particuliers p1(1,"BETY","bety@gmail.com", "12 rue des Oliviers", "",  "94000",  "CRETEIL"  ,"12/11/1985" ,"Daniel" ,"Masculin" );
    Particuliers p2(3,"BODIN", "bodin@gmail.com", "10, rue des Olivies", "Etage 2", "94300" , "VINCENNES", "05/05/1965",  "Justin",  "Masculin" );
    Particuliers p3(5, "BERRIS", " berris@gmail.com" ,"15, rue de la R�publique", "" ,  "FONTENAY SOUS BOIS", "94120", "06/06/1977",  "Karine", "F�minin"  );
    Particuliers p4(7, "ABENIR", "abenir@gmail.com ", "25, rue de la Paix", "", "92100",  "LA DEFENSE", "12/04/1977",  "Alexandra", "F�minin" );
    Particuliers p5(9, "BENSAID", "bensaid@gmail.com", "3, avenue des Parcs", "", "93500",  "ROISSY EN France" , "16/04/1976", "Georgia ", "F�minin");
    Particuliers p6(11, "ABABOU", "ababou@gmail.com", "3, rue Lecourbe", "", "93200",  "BAGNOLET",  "10/10/1970", "Teddy", "Masculin"  );

    Professionnels p7(2,"AXA","info@axa.fr","125 rue LaFayette ", "Digicode 1432", "94120", "FONTENAY SOUS BOIS", "12548795641122", "SARL", "125 rue LaFayette ", "Digicode 1432", "94120", "FONTENAY SOUS BOIS");
    Professionnels p8(4,"PAUL", "info@paul.fr", "36, quai des Orf�vres", "",   "93500", "ROISSY EN France",  "87459564455444",  "EURL", "10, esplanade de la D�fense", "", "92060", "LA DEFENSE" );
    Professionnels p9(6, "PRIMARK", "contact@primark.fr", "32, rue E. Renan", "Bat. C", "75002", "PARIS",  "08755897458455", "SARL", "32, rue E. Renan", "Bat. C", "75002", "PARIS" );
    Professionnels p10(8, "ZARA", "info@zara.fr", "23, av P. Valery", "",   "92100",  "LA DEFENSE",  "65895874587854", "SA", "24, esplanade de la D�fense", "Tour Franklin", "92060", "LA DEFENSE" );
    Professionnels p11(10, "LEONIDAS", "contact@leonidas.fr", "15, Place de la Bastille", "Fond de Cour", "75003", "PARIS",  "91235987456832" , "SAS", "10, rue de la Paix", "",   "75008", "PARIS");

    //Ajout des clients dans la liste
    ListeClients.Ajouter(&p1);
    ListeClients.Ajouter(&p2);
    ListeClients.Ajouter(&p3);
    ListeClients.Ajouter(&p4);
    ListeClients.Ajouter(&p5);
    ListeClients.Ajouter(&p6);
    ListeClients.Ajouter(&p7);
    ListeClients.Ajouter(&p8);
    ListeClients.Ajouter(&p9);
    ListeClients.Ajouter(&p10);
    ListeClients.Ajouter(&p11);

    //Affichage du contenu du vecteur
    cout << ListeClients.AfficherTout();

    Lecture opp;

    opp.LireFichier();

  cout << opp.toString();
}

    catch(MonException& ex)
    {

        cout << "MonException(what) : " <<  ex.what() << endl;
    }

     catch(...) //Cas Fourre tout
    {
        cout << "Autre Erreur !! " << endl;
    }


    return 0;

}
