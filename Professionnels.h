    #ifndef PROFESSIONNELS_H
    #define PROFESSIONNELS_H
    #include <string>
    #include <sstream>
    #include <Client.h>
    #include <iostream>
    using namespace std;

    enum StatutJuridique{SARL, SA, SAS, EURL};
    class Professionnels : public Client
    {
        public:

             Professionnels(int =0, string ="\0", string="\0", string ="\0", string ="\0", string="\0", string="\0",string="/0",string="\0", string ="\0", string ="\0", string ="\0", string ="\0");
            virtual ~Professionnels();

            string GetSiret() {  return Siret;
            }

             void SetSiret(string val)
            {

                if(val.length()>14)
                throw MonException(3);
                Siret = val;
            }

            string GetStatut(){return StatutJ;}
            void SetStatut(string val)
            {
                if(val=="SA" && val=="SAS" && val=="SARL" && val=="EURL")
                {
                    throw MonException(4);

                }

                StatutJ = val;
            }
            virtual string toString() override;
        protected:

        private:
            string Siret;
            string StatutJ;
            Adresse *AdresseSiege=nullptr;
    };

    #endif // PROFESSIONNELS_H
